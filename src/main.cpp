#include <iostream>
#include "Stack.hpp"
#include <string>
#include <cmath>

int main(int argc, char* argv[])
{
    Stack<double> s(100);
    while(1)
    {
        std::string in;
        std::getline(std::cin,in);
        if(in == "+" && s.getSize() >= 2)
        {
            double num2 = s.pop();
            double num1 = s.pop();
            double res = num1+num2;
            s.push(res);
        }else if(in == "-" && s.getSize() >= 2)
        {
            double num2 = s.pop();
            double num1 = s.pop();
            double res = num1-num2;
            s.push(res);
        }else if(in == "*" && s.getSize() >= 2)
        {
            double num2 = s.pop();
            double num1 = s.pop();
            double res = num1*num2;
            s.push(res);
        }else if(in == "/" && s.getSize() >= 2)
        {
            double num2 = s.pop();
            double num1 = s.pop();
            double res = num1/num2;
            s.push(res);
        }else if(in == "%" && s.getSize() >= 2)
        {
            double num2 = s.pop();
            double num1 = s.pop();
            double res = std::fmod(num1, num2);
            s.push(res);
        }else if(in == "^" && s.getSize() >= 2)
        {
            double num2 = s.pop();
            double num1 = s.pop();
            double res = std::pow(num1, num2);
            s.push(res);
        }else if(in == "p")
        {
            double temp[s.getSize()];
            int size = s.getSize();
            std::cout << "-----------" << std::endl;
            for(int i=0; i<size; i++)
            {
                temp[i] = s.pop();
            }
            for(int i=size-1; i>=0; i--)
            {
                std::cout << temp[i] << std::endl;
                s.push(temp[i]);
            }
            std::cout << "-----------" << std::endl;
        }else if(in == "?")
        {
            std::cout << "-----------" << std::endl
                << "'?' - Help" << std::endl
                << "'[number]' - Put the number into the stack" << std::endl
                << "'[+,-,/,%,*,^]' - Do the operation on the 2 bottom most numbers" << std::endl
                << "'p' - Print out the stack" << std::endl
                << "-----------" << std::endl;
        }else if(in.empty() || in == "" || in.find_first_not_of(' ') == std::string::npos) 
        {
            //Skip
            continue;
        }else
        {
            //NOTE: This does not check any other text input
            double num = std::stod(in);
            s.push(num);
        }
    }
    return 0;
}