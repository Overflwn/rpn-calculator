#ifndef STACK_HPP
#define STACK_HPP

template<typename T>
class Stack
{
private:
    int currentSize;
    int maxSize;
    T* data;
public:
    Stack(int size);
    ~Stack();
    bool push(T data);
    T pop();
    int getSize();
};

template<typename T>
Stack<T>::Stack(int size) : data(new T[size]), maxSize(size), currentSize(0)
{
}

template<typename T>
Stack<T>::~Stack()
{
    delete[] this->data;
}

template<typename T>
bool Stack<T>::push(T data)
{
    if (currentSize == maxSize)
        return false;
    
    this->data[++currentSize] = data;
    return true;
}

template<typename T>
T Stack<T>::pop()
{
    if (currentSize == 0)
        return 0;
    
    return data[currentSize--];
}

template<typename T>
int Stack<T>::getSize()
{
    return currentSize;
}

#endif